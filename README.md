<p align="center">
<img src="logo/dromara/Dromara_透明底@1x.png" height="220"/>
</p>

<div align="center">

**一个由顶尖开源项目维护者自发组织的开源社区，让每一位开源爱好者，体会到开源的快乐。**

> 为往圣继绝学，一个人或许能走的更快，但一群人会走的更远。

</div> 

## 社区介绍

**Dromara** 是由顶尖开源项目维护者自发组织的开源社区。提供包括流行工具，安全认证，调度编排，前端和应用框架，AI等开源产品，累计获得 300K+ Star，服务成千上万团队。技术栈全面开源共建，保持社区中立。希望让每一位开源爱好者，体会到开源的快乐。

## 我们的愿景

让每一位开源爱好者，体会到开源的快乐。

## 社区口号

技术栈全面开源共建、保持社区中立、和谐快乐做开源 。

## 官网

**[https://dromara.org](https://dromara.org)** 是 **Dromara** 开源社区官方网站。

**[https://dromara.org.cn](https://dromara.org.cn)** 是 **Dromara** 备用网站。

## 官方活动

**在 [Dromara社区文档仓库](https://dromara.org/zh/activity) 了解我们每一次的会议与活动信息。**

## 加入知识星球

<img src="assets/haibao.png" height="360"/>


## 社区项目

**[正式项目](https://dromara.org/zh/projects)** 是 **Dromara开源组织** 正式项目的列表。

## 项目孵化

**[孵化器官网](https://incubator.dromara.org)** 是 **Dromara开源组织** 孵化器官网。


## 组织责任

- 组织不得从事违法或损人利己的事情
- 负责社区新旧捐赠项目评审工作
- 负责新旧社区成员管理工作
- 负责社区下所有孵化项目推广，宣传和项目版本更新日志维护
- 负责统筹和执行社区组织的活动

## 行为准则


- 社区成员准则：不得从事违法或损人利己的事情。

- 社区项目：不得从事违法或损人利己的事情



## 赞助/支持

**Dromara** 社区能够持续运营和提供更加优质的服务离不开大家的支持。如果想成为 **Dromara** 社区赞助商或支持者，请考虑支持：

<img src="assets/donation.png" height="360">

**Dromara** 社区承诺将收到的所有赞助支持资金完全公开化，且后续资金用途仅限于 **Dromara** 社区运营支出。

**[点击查看全部赞助商](https://dromara.org/zh/donation)**

## 联系我们

- 捐赠项目/加入组织：[xiaoyu@dromara.org](mailto:xiaoyu@dromara.org)
